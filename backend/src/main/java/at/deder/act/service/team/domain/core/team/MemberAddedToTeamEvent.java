package at.deder.act.service.team.domain.core.team;

import at.deder.act.service.team.domain.framework.DomainEvent;

public class MemberAddedToTeamEvent implements DomainEvent {
    private final String teamId;
    private final String personId;

    public MemberAddedToTeamEvent(String teamId, String personId) {

        this.teamId = teamId;
        this.personId = personId;
    }

    public String getTeamId() {
        return teamId;
    }

    public String getMemberId() {
        return personId;
    }
}
