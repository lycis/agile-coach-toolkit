package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.people.PersonRepository;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

import static spark.Spark.halt;

public class DeletePersonHandler extends BaseRequestHandler {
    private PersonRepository repository;

    @Inject
    public DeletePersonHandler(PersonRepository repository) {
        this.repository = repository;
    }

    public String handle(Request request, Response response) {
        var id = request.params("id");
        if(id.isEmpty()) {
            haltWithError(HttpStatus.BAD_REQUEST_400, "Missing ID", "An ID of an existing person is required.");
        }

        var person = repository.getPersonById(id);
        if(person == null) {
            halt(HttpStatus.NOT_FOUND_404);
        }
        if(!person.isActive()) {
            haltWithError(HttpStatus.NOT_FOUND_404, "Person is already inactive", "The requested person is already inactive.");
        }

        repository.deletePerson(id);
        response.status(HttpStatus.OK_200);
        return "";
    }

}
