package at.deder.act.service.team.outbound.audit;

public class ConsoleAuditLog implements AuditLogAdapter {
    @Override
    public void writeAuditLog(String record) {
        System.out.println(record);
    }
}
