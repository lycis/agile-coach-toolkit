package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.command.RegisterNewTeam;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import com.google.gson.JsonObject;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

class CreateTeamHandler {
    private final TeamRepository repository;
    private final DomainEventDispatcher eventDispatcher;

    @Inject
    CreateTeamHandler(TeamRepository repository, DomainEventDispatcher eventDispatcher) {
        this.repository = repository;
        this.eventDispatcher = eventDispatcher;
    }

    Response handleRequest(Request request, Response response) {
        CreateTeamRequestData requestData = CreateTeamRequestData.parse(request, response);
        if(!requestData.isValid()) {
            return response;
        }

        var name = requestData.getName();
        var cmd = new RegisterNewTeam(repository, name, eventDispatcher);
        var team = cmd.register();

        response.status(HttpStatus.OK_200);

        var json = new JsonObject();
        json.addProperty("id", team.getId());
        response.body(json.toString());
        return response;
    }
}
