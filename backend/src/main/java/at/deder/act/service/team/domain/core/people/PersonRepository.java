package at.deder.act.service.team.domain.core.people;

import java.util.Collection;

public interface PersonRepository {
    void savePerson(Person target);
    Person createPerson();
    Collection<String> queryAllPersonIds();
    Person getPersonById(String id);
    void deletePerson(String id);
}
