package at.deder.act.service.team.domain.framework;

import java.util.ArrayList;
import java.util.List;

public abstract class Entity {

    private transient List<DomainEvent> pendingEvents = new ArrayList<>();

    protected void raiseEvent(DomainEvent e) {
        pendingEvents.add(e);
    }

    public List<DomainEvent> getRaisedDomainEvents() {
        return pendingEvents;
    }
}
