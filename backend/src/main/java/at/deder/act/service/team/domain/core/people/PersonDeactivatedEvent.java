package at.deder.act.service.team.domain.core.people;

import at.deder.act.service.team.domain.framework.DomainEvent;

public class PersonDeactivatedEvent implements DomainEvent {
    private String id;

    public PersonDeactivatedEvent(String id) {
        this.id = id;
    }

    public String getPersonId() {
        return id;
    }
}
