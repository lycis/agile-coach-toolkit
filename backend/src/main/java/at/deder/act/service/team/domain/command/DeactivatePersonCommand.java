package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import com.google.inject.Inject;

public class DeactivatePersonCommand {
    private final PersonRepository repository;
    private DomainEventDispatcher eventDispatcher;
    private String personId;

    @Inject
    public DeactivatePersonCommand(PersonRepository repository, DomainEventDispatcher mockDispatcher, String personId) {
        this.repository = repository;
        this.eventDispatcher = mockDispatcher;
        this.personId = personId;
    }

    public void execute() {
        Person person = repository.getPersonById(personId);
        if(person == null) {
            return;
        }
        if(!person.isActive()) {
            dispatchEventsFor(person);
            return;
        }

        repository.deletePerson(personId);
        dispatchEventsFor(person);
    }

    private void dispatchEventsFor(Person person) {
        person.getRaisedDomainEvents().forEach(eventDispatcher::dispatch);
    }
}
