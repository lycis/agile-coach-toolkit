package at.deder.act.service.team.inbound.rest;

import com.google.gson.JsonObject;
import spark.Request;
import spark.Response;

class CreateTeamRequestData extends AbstractRequestData {
    private String name;

    static CreateTeamRequestData parse(Request request, Response response) {
        var data = new CreateTeamRequestData();

        JsonObject root = convertFromJson(request);
        if (root == null) {
            data.invalidate(response);
            return data;
        }

        var nameJson = root.get("name");
        if(nameJson == null) {
            data.invalidate(response);
            return data;
        }

        data.name = nameJson.getAsString();
        data.setValid();
        return data;
    }

    public String getName() {
        return name;
    }
}
