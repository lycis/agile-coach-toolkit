package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.command.AddNoteToTeam;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

import java.util.List;

public class AppendNoteHandler {
    private TeamRepository mockRepository;

    @Inject
    public AppendNoteHandler(TeamRepository mockRepository) {
        this.mockRepository = mockRepository;
    }

    public Response handle(Request request, Response response) {
        var requestData = AppendNoteData.parse(request, response);
        if(!requestData.isValid()) {
            return response;
        }

        var command = new AddNoteToTeam(mockRepository, requestData.getId());
        List<String> notesToAdd = requestData.getNotesToAdd();
        notesToAdd.forEach(command::addNote);
        command.submit();

        response.status(HttpStatus.OK_200);
        return response;
    }
}
