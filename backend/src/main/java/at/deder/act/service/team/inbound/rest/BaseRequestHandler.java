package at.deder.act.service.team.inbound.rest;

import com.google.gson.JsonObject;
import spark.Request;
import spark.Response;

import static spark.Spark.halt;

public abstract class BaseRequestHandler {
    protected void haltWithError(int errorCode, String title, String detail) {
        halt(errorCode, buildErrorJson(title, detail).toString());
    }

    private JsonObject buildErrorJson(String title, String detail) {
        var errorJson = new JsonObject();
        errorJson.addProperty("title", title);
        errorJson.addProperty("detail", detail);
        return errorJson;
    }

    public abstract String handle(Request request, Response response);
}
