package at.deder.act.service.team.outbound.audit;

import at.deder.act.service.team.domain.core.people.PersonDeactivatedEvent;
import at.deder.act.service.team.domain.core.team.MemberAddedToTeamEvent;
import at.deder.act.service.team.domain.core.team.TeamCreatedEvent;
import at.deder.act.service.team.domain.core.team.TeamRenamedEvent;
import at.deder.act.service.team.domain.framework.DomainEvent;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import com.google.gson.Gson;
import com.google.inject.Inject;

public class AuditLogService {
    private DomainEventDispatcher eventDispatch;
    private final AuditLogAdapter auditLog;

    @Inject
    AuditLogService(DomainEventDispatcher eventDispatch, AuditLogAdapter auditLog) {
        this.eventDispatch = eventDispatch;
        this.auditLog = auditLog;
    }

    private void logEvent(DomainEvent event) {
        var gson = new Gson();
        var json = gson.toJsonTree(event);
        json.getAsJsonObject().addProperty("_type", event.getClass().getCanonicalName());
        auditLog.writeAuditLog(json.toString());
    }

    public void start() {
        eventDispatch.on(TeamRenamedEvent.class, this::logEvent);
        eventDispatch.on(TeamCreatedEvent.class, this::logEvent);
        eventDispatch.on(PersonDeactivatedEvent.class, this::logEvent);
        eventDispatch.on(MemberAddedToTeamEvent.class, this::logEvent);
    }
}
