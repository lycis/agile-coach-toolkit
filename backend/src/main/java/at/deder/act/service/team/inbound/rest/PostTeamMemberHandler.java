package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.command.AddTeamMember;
import at.deder.act.service.team.domain.command.PersonNotFoundException;
import at.deder.act.service.team.domain.command.TeamNotFoundException;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

public class PostTeamMemberHandler extends BaseRequestHandler {
    private TeamRepository teamRepo;
    private PersonRepository peopleRepo;
    private DomainEventDispatcher eventDispatcher;

    @Inject
    public PostTeamMemberHandler(TeamRepository teamRepo, PersonRepository peopleRepo, DomainEventDispatcher eventDispatcher) {
        this.teamRepo = teamRepo;
        this.peopleRepo = peopleRepo;
        this.eventDispatcher = eventDispatcher;
    }

    @Override
    public String handle(Request request, Response response) {
        var teamId = request.params("team");
        var personId = request.params("person");

        var command = new AddTeamMember(teamRepo, peopleRepo, eventDispatcher);
        command.addMemberToTeam(teamId, personId);
        try {
            command.execute();
        } catch(TeamNotFoundException e) {
            haltWithError(HttpStatus.NOT_FOUND_404, "Team not found", "A team with the given team-id does not exist");
        } catch(PersonNotFoundException e) {
            haltWithError(HttpStatus.UNPROCESSABLE_ENTITY_422, "Person does not exist", "The given person-id does not map to an existing or active person.");
        }

        response.status(HttpStatus.OK_200);
        return "";
    }
}
