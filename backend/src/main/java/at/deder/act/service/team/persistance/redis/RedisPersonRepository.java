package at.deder.act.service.team.persistance.redis;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import at.deder.act.service.team.persistance.IdGenerator;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.inject.Inject;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class RedisPersonRepository implements PersonRepository {
    private RedisConnection connection;
    private IdGenerator idGenerator;

    @Inject
    public RedisPersonRepository(RedisConnection connection, IdGenerator idGenerator) {
        this.connection = connection;
        this.idGenerator = idGenerator;
    }

    synchronized public void savePerson(Person person) {
        canSavePerson(person);

        var gson = new Gson();
        var json = gson.toJson(person);

        connection.addToSet("person:ids", person.getId());
        connection.set(String.format("person:%s", person.getId()), json);
    }

    @Override
    synchronized public Person createPerson() {
        var person = new Person();
        person.setId(idGenerator.generateId());
        person.setActive(true);
        return person;
    }

    @Override
    synchronized public Collection<String> queryAllPersonIds() {
        var personIds = connection.getMembersOfSet("person:ids");
        return personIds.stream().map(this::getPersonById).filter(person -> person != null && person.isActive()).map(Person::getId).collect(Collectors.toSet());
    }

    @Override
    synchronized public Person getPersonById(String id) {
        var json = connection.get(String.format("person:%s", id));
        if(json == null) {
            return null;
        }

        Person person = convertJsonToPerson(json);
        if(!person.isActive()) {
            return null;
        }

        return person;
    }

    @Override
    synchronized public void deletePerson(String id) {
        var person = getPersonById(id);
        if(person == null) {
            return;
        }

        person.setActive(false);
        savePerson(person);
    }

    private Person convertJsonToPerson(String json) {
        var gson = new Gson();
        Type typeToken = new TypeToken<Person>() {}.getType();
        return gson.fromJson(json, typeToken);
    }

    private void canSavePerson(Person person) {
        if(person.getName() == null) {
            throw new IllegalArgumentException("Person requires a name");
        }
        if(person.getId() == null) {
            throw new IllegalArgumentException("Person requires ID");
        }
    }
}
