package at.deder.act.service.team.domain.core.people;

import at.deder.act.service.team.domain.framework.Entity;
import org.apache.commons.lang3.Validate;

public class Person extends Entity {
    private Name name;
    private String id;
    private boolean active;

    public void setName(Name name) {
        Validate.notNull(name);
        this.name = name;
    }

    public Name getName() {
        return name;
    }

    public void setId(String id) {
        Validate.notNull(id);
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setActive(boolean activeStatus) {
        if(this.active && !activeStatus) {
            raiseEvent(new PersonDeactivatedEvent(getId()));
        }
        this.active = activeStatus;
    }

    public boolean isActive() {
        return active;
    }
}
