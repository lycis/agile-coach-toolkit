package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.team.Note;
import at.deder.act.service.team.persistance.NoteJsonAdapter;
import at.deder.act.service.team.domain.query.TeamQueryService;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;

public class GetTeamNotesHandler {
    private final TeamQueryService queryService;

    @Inject
    public GetTeamNotesHandler(TeamQueryService queryService) {
        this.queryService = queryService;
    }

    public String handle(Request request, Response response) {
        var id = request.params("id");

        var notes = queryService.queryNotesForTeam(id);
        if(notes == null) {
            response.status(HttpStatus.NOT_FOUND_404);
            return null;
        }

        response.status(HttpStatus.OK_200);
        var builder = new GsonBuilder().registerTypeAdapter(Note.class, new NoteJsonAdapter());
        return builder.create().toJson(notes);
    }
}
