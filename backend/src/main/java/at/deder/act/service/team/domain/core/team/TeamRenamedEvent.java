package at.deder.act.service.team.domain.core.team;

import at.deder.act.service.team.domain.framework.DomainEvent;

public class TeamRenamedEvent implements DomainEvent {
    final private String teamId;
    final private String oldName;
    final private String newName;

    public TeamRenamedEvent(String teamId, String oldName, String newName) {

        this.teamId = teamId;
        this.oldName = oldName;
        this.newName = newName;
    }

    public String getOldName() {
        return oldName;
    }

    public String getNewName() {
        return newName;
    }

    public String getTeamId() {
        return teamId;
    }
}
