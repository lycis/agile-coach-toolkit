package at.deder.act.service.team;

import at.deder.act.service.team.domain.core.people.PersonRepository;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import at.deder.act.service.team.outbound.audit.AuditLogAdapter;
import at.deder.act.service.team.outbound.audit.ConsoleAuditLog;
import at.deder.act.service.team.outbound.dispatch.InMemoryEventDispatcher;
import at.deder.act.service.team.persistance.RandomUUIDIdGenerator;
import at.deder.act.service.team.persistance.IdGenerator;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import at.deder.act.service.team.persistance.redis.RedisAdapter;
import at.deder.act.service.team.persistance.redis.RedisConnection;
import at.deder.act.service.team.persistance.redis.RedisPersonRepository;
import at.deder.act.service.team.persistance.redis.RedisTeamRepository;
import at.deder.act.service.team.domain.query.TeamQueryService;
import at.deder.act.service.team.domain.query.TeamQueryServiceImpl;
import com.google.inject.AbstractModule;

public class ApplicationModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(TeamRepository.class).to(RedisTeamRepository.class);
        bind(RedisConnection.class).to(RedisAdapter.class);
        bind(IdGenerator.class).to(RandomUUIDIdGenerator.class);
        bind(TeamQueryService.class).to(TeamQueryServiceImpl.class);
        bind(DomainEventDispatcher.class).toInstance(new InMemoryEventDispatcher());
        bind(AuditLogAdapter.class).to(ConsoleAuditLog.class);
        bind(PersonRepository.class).to(RedisPersonRepository.class);

    }
}
