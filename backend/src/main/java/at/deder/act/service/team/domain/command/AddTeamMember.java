package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.PersonRepository;
import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;

import java.util.ArrayList;
import java.util.List;

public class AddTeamMember {
    private final TeamRepository teamRepository;
    private final PersonRepository personRepository;
    private DomainEventDispatcher eventDispatcher;
    private String teamId;
    private List<String> memberIds = new ArrayList<>();

    public AddTeamMember(TeamRepository teamRepository, PersonRepository personRepository, DomainEventDispatcher eventDispatcher) {
        this.teamRepository = teamRepository;
        this.personRepository = personRepository;
        this.eventDispatcher = eventDispatcher;
    }

    public void addMemberToTeam(String teamId, String personId) {
        this.teamId = teamId;
        this.memberIds.add(personId);
    }

    public void execute() {
        var team = teamRepository.findTeam(teamId);
        validateTeamAndPersons(team);

        memberIds.forEach(team::addMember);
        team.getRaisedDomainEvents().forEach(eventDispatcher::dispatch);
        teamRepository.saveTeam(team);
    }

    private void validateTeamAndPersons(Team team) {
        if(team == null) {
            throw new TeamNotFoundException(String.format("Team (%s) does not exist", teamId));
        }
        for(String personId: memberIds) {
            if (personDoesNotExist(personId)) {
                throw new PersonNotFoundException(String.format("Person (%s) does not exist", personId));
            }
        }
    }

    private boolean personDoesNotExist(String id) {
        return personRepository.getPersonById(id) == null;
    }
}
