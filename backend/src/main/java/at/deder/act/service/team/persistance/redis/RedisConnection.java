package at.deder.act.service.team.persistance.redis;

import java.util.Set;

public interface RedisConnection {
    void selectDatabase(int db);

    void set(String key, String value);

    void addToSet(String keyValid, String value);

    Set<String> getMembersOfSet(String key);

    String get(String key);

    boolean isMemberOfSet(String keyValid, String value);
}
