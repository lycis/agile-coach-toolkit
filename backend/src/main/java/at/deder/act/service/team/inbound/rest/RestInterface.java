package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.ApplicationModule;
import at.deder.act.service.team.domain.query.PersonQueryService;
import com.google.inject.Guice;
import com.google.inject.Injector;

import static spark.Spark.*;

public class RestInterface {
    private final Injector injector;

    public RestInterface(Injector injector) {
        this.injector = injector;
    }

    public void start() {
        initExceptionHandler(Throwable::printStackTrace);

        enableCORS();

        teamListHandlers();
        teamHandler();

        personHandler();
    }

    private void personHandler() {
        get("/person", new GetPersonListHandler(injector.getInstance(PersonQueryService.class))::handle);
        put("/person", injector.getInstance(CreatePersonHandler.class)::handle);
        delete("/person/:id", injector.getInstance(DeletePersonHandler.class)::handle);
    }

    private void teamHandler() {
        patch("/team/:id/notes", injector.getInstance(AppendNoteHandler.class)::handle);
        get("/team/:id/notes",   injector.getInstance(GetTeamNotesHandler.class)::handle);
        get("/team/:id/name",    injector.getInstance(GetTeamnameHandler.class)::handle);
        post("/team/:id/name",   injector.getInstance(RenameTeamHandler.class)::handleTeamRenameRequest);
        post("/team/:team/members/:person", injector.getInstance(PostTeamMemberHandler.class)::handle);
    }

    private void teamListHandlers() {
        put("/team", injector.getInstance(CreateTeamHandler.class)::handleRequest);
        get("/team", injector.getInstance(GetTeamsHandler.class)::handleRequest);
    }

    private void enableCORS() {
        options("/*", (request, response) -> {
            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });
        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));
    }
}
