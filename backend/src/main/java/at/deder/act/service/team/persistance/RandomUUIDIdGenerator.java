package at.deder.act.service.team.persistance;

import java.util.UUID;

public class RandomUUIDIdGenerator implements IdGenerator {
    RandomUUIDIdGenerator() {
    }

    @Override
    public String generateId() {
        return UUID.randomUUID().toString();
    }
}
