package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import com.google.inject.Inject;

public class RenameTeam {
    private TeamRepository teamRepo;
    private DomainEventDispatcher dispatcher;

    @Inject
    public RenameTeam(TeamRepository teamRepo, DomainEventDispatcher dispatcher) {
        this.teamRepo = teamRepo;
        this.dispatcher = dispatcher;
    }

    public void renameTeam(String id, String newName) throws RenamedTeamDoesNotExist {
        Team teamToRename = teamRepo.findTeam(id);
        if(teamToRename == null) {
            throw new RenamedTeamDoesNotExist();
        }

        teamToRename.setName(newName);

        teamToRename.getRaisedDomainEvents().forEach(event -> dispatcher.dispatch(event));

        teamRepo.saveTeam(teamToRename);
    }
}
