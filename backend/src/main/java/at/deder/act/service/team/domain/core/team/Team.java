package at.deder.act.service.team.domain.core.team;

import at.deder.act.service.team.domain.framework.Entity;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.List;

public class Team extends Entity {
    List<String> members = null;
    private String name;
    private String id;
    private List<Note> notes;

    private Team() {
        notes = new ArrayList<>();
    }

    public static Team create(String name) {
        var team = new Team();
        team.setName(name);

        return team;
    }

    public void addMember(String personId)  {
        if(members == null) {
            members = new ArrayList<>();
        }

        if(members.contains(personId)) {
            throw new DuplicateMemberException();
        }

        members.add(personId);
        raiseEvent(new MemberAddedToTeamEvent(this.id, personId));
    }

    public boolean isMember(String memberName) {
        return members.contains(memberName);
    }

    public void setName(String name) {
        Validate.notNull(name);

        raiseNameChangeEventIfTeamNameChanged(name);

        this.name = name;
    }

    private void raiseNameChangeEventIfTeamNameChanged(String name) {
        if(this.name != null && !name.equals(this.name)) {
            raiseEvent(new TeamRenamedEvent(id, this.name, name));
        }
    }

    public String getName() {
        return this.name;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        if(id == null) {
            throw new IllegalArgumentException();
        }
        this.id = id;
    }

    public void addNote(Note note) {
        Validate.notBlank(note.getContent());
        notes.add(note);
    }

    public List<Note> getNotes() {
        return notes;
    }

    public List<String> getMembers() {
        return members;
    }
}
