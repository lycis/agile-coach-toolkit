package at.deder.act.service.team.outbound.dispatch;

import at.deder.act.service.team.domain.framework.DomainEvent;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class InMemoryEventDispatcher implements DomainEventDispatcher {
    Map<Class<? extends DomainEvent>, List<Consumer<DomainEvent>>> subscribers = new HashMap<>();

    @Override
    public void dispatch(DomainEvent event) {
        Class<? extends DomainEvent> type = event.getClass();

        if(hasNoSubscribersFor(type)) {
            return;
        }

        dispatchEventToSubscribers(event, type);
    }

    private void dispatchEventToSubscribers(DomainEvent event, Class<? extends DomainEvent> type) {
        subscribers.get(type).forEach(domainEventConsumer -> domainEventConsumer.accept(event));
    }

    private boolean hasNoSubscribersFor(Class<? extends DomainEvent> aClass) {
        return !subscribers.containsKey(aClass);
    }

    @Override
    public void on(Class<? extends DomainEvent> type, Consumer<DomainEvent> function) {
        if(hasNoSubscribersFor(type)) {
            subscribers.put(type, new ArrayList<>());
        }

        subscribers.get(type).add(function);
    }

    @Override
    public void dispatchAll(List<DomainEvent> raisedDomainEvents) {
        raisedDomainEvents.forEach(this::dispatch);
    }
}
