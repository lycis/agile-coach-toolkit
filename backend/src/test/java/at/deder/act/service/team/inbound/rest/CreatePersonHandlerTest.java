package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import spark.HaltException;
import spark.Request;
import spark.Response;

import static at.deder.act.service.team.inbound.rest.RestApiAssertions.assertRestApiError;
import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class CreatePersonHandlerTest {
    @Test
    public void putting_name_of_person_returns_id() {
        var request = mock(Request.class);
        when(request.body()).thenReturn("{\"name\": \"Max Mustermann\"}");
        var response = mock(Response.class);
        var mockRepo = mock(PersonRepository.class);
        var mockPerson = mock(Person.class);
        when(mockRepo.createPerson()).thenReturn(mockPerson);
        when(mockPerson.getId()).thenReturn("person-test-id");

        var handler = new CreatePersonHandler(mockRepo);
        var result = handler.handle(request, response);

        verify(response).status(HttpStatus.OK_200);
        verify(mockRepo).createPerson();
        verify(mockRepo).savePerson(mockPerson);
        assertThatJson(result).inPath("$.id").isEqualTo("person-test-id");
    }

    @Test
    public void empty_payload_fails() {
        var request = mock(Request.class);
        when(request.body()).thenReturn("");
        var response = mock(Response.class);
        var mockRepo = mock(PersonRepository.class);

        var handler = new CreatePersonHandler(mockRepo);

        assertRestApiError(() ->  handler.handle(request, response), HttpStatus.BAD_REQUEST_400, "Missing payload", "This call requires the name for the new person.");
    }


    @Test
    public void missing_name_fails() {
        var request = mock(Request.class);
        when(request.body()).thenReturn("{\"something\": \"but no name\"}");
        var response = mock(Response.class);
        var mockRepo = mock(PersonRepository.class);

        var handler = new CreatePersonHandler(mockRepo);
        var exception = assertThrows(HaltException.class, () -> handler.handle(request, response));

        assertRestApiError(() ->  handler.handle(request, response), HttpStatus.BAD_REQUEST_400, "Missing payload", "This call requires the name for the new person.");
    }


    @Test
    public void invalid_name_fails() {
        var request = mock(Request.class);
        when(request.body()).thenReturn("{\"name\": \"   \"}");
        var response = mock(Response.class);
        var mockRepo = mock(PersonRepository.class);

        var handler = new CreatePersonHandler(mockRepo);
        var exception = assertThrows(HaltException.class, () -> handler.handle(request, response));

        assertThat(exception.statusCode()).isEqualTo(HttpStatus.BAD_REQUEST_400);
        // TODO test rest api conform errors (assertRestApiError)
    }
}
