package at.deder.act.service.team.persistance.redis;

import at.deder.act.service.team.domain.core.people.Name;
import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.persistance.IdGenerator;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.mockito.Mockito.*;

public class RedisPersonRepositoryTest {
    @Test
    public void person_with_name_and_id_can_be_stored() {
        var mockRedis = mock(RedisConnection.class);

        savePerson(mockRedis, true, "Max Mustermann", "test-id");

        assertIdAddedToIdSet(mockRedis, "test-id");
        var captorKey = ArgumentCaptor.forClass(String.class);
        var captorValue = ArgumentCaptor.forClass(String.class);
        verify(mockRedis).set(captorKey.capture(), captorValue.capture());

        assertThat(captorKey.getValue()).isEqualTo("person:test-id");
        assertThatJson(captorValue.getValue()).inPath("$.name.name").isEqualTo("Max Mustermann");
    }

    private void savePerson(RedisConnection mockRedis, boolean active, String name, String id) {
        var person = new Person();
        person.setName(new Name(name));
        person.setId(id);
        person.setActive(active);

        var repo = new RedisPersonRepository(mockRedis, mock(IdGenerator.class));
        repo.savePerson(person);
    }

    @Test
    public void active_flag_is_persisted() {
        active_person_has_active_true();
        inactive_person_has_active_false();
    }

    private void inactive_person_has_active_false() {
        RedisConnection mockRedis;
        ArgumentCaptor<String> captorKey;
        ArgumentCaptor<String> captorValue;
        mockRedis = mock(RedisConnection.class);

        savePerson(mockRedis, false, "Nina Mustermann", "inactive-person");

        assertIdAddedToIdSet(mockRedis, "inactive-person");
        captorKey = ArgumentCaptor.forClass(String.class);
        captorValue = ArgumentCaptor.forClass(String.class);
        verify(mockRedis).set(captorKey.capture(), captorValue.capture());
        assertThatJson(captorValue.getValue()).inPath("$.active").isEqualTo("false");
    }

    private void active_person_has_active_true() {
        var mockRedis = mock(RedisConnection.class);

        savePerson(mockRedis, true, "Max Mustermann", "active-person");

        assertIdAddedToIdSet(mockRedis, "active-person");
        var captorKey = ArgumentCaptor.forClass(String.class);
        var captorValue = ArgumentCaptor.forClass(String.class);
        verify(mockRedis).set(captorKey.capture(), captorValue.capture());
        assertThatJson(captorValue.getValue()).inPath("$.active").isEqualTo("true");
    }

    private void assertIdAddedToIdSet(RedisConnection mockRedis, String id) {
        verify(mockRedis).addToSet("person:ids", id);
    }

    @Test
    public void person_without_name_cannot_be_stored() {
        var person = new Person();
        person.setId("test-id");
        var mockRedis = mock(RedisConnection.class);

        var repo = new RedisPersonRepository(mockRedis, mock(IdGenerator.class));
        assertThrows(IllegalArgumentException.class, () -> repo.savePerson(person));
    }

    @Test
    public void person_without_id_cannot_be_stored() {
        var person = new Person();
        person.setName(new Name("foo"));

        var repo = new RedisPersonRepository(mock(RedisConnection.class), mock(IdGenerator.class));
        assertThrows(IllegalArgumentException.class, () -> repo.savePerson(person));
    }

    @Test
    public void creating_a_person_sets_id() {
        var mockIdGenerator = mock(IdGenerator.class);
        when(mockIdGenerator.generateId()).thenReturn("test-id");
        var repo = new RedisPersonRepository(mock(RedisConnection.class), mockIdGenerator);

        var result = repo.createPerson();

        assertThat(result.getId()).isEqualTo("test-id");
    }

    @Test
    public void getting_person_by_id_restores_full_person() {
        var mockConnection = mock(RedisConnection.class);
        when(mockConnection.get("person:test-id")).thenReturn("{ \"id\": \"test-id\", \"name\": { \"name\": \"Max Mustermann\" }, \"active\": true }");

        var repo = new RedisPersonRepository(mockConnection, mock(IdGenerator.class));
        var result = repo.getPersonById("test-id");

        assertThat(result.getId()).isEqualTo("test-id");
        assertThat(result.getName().asString()).isEqualTo("Max Mustermann");
        assertThat(result.isActive()).isTrue();
    }

    @Test
    public void getting_not_existing_person_returns_null() {
        var mockConnection = mock(RedisConnection.class);
        when(mockConnection.get("test-id")).thenReturn(null);

        var repo = new RedisPersonRepository(mockConnection, mock(IdGenerator.class));
        var result = repo.getPersonById("test-id");

        assertThat(result).isNull();
    }

    @Test
    public void deleting_active_person_sets_it_to_deactivated() {
        var mockConnection = mock(RedisConnection.class);
        when(mockConnection.get("person:test-id")).thenReturn("{ \"id\": \"test-id\", \"name\": { \"name\": \"Test Mann\" }, \"active\": true }");

        var repo = new RedisPersonRepository(mockConnection, null);
        repo.deletePerson("test-id");

        verify(mockConnection).get("person:test-id");

        var valueCaptor = ArgumentCaptor.forClass(String.class);
        var keyCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockConnection).set(keyCaptor.capture(), valueCaptor.capture());
        assertThatJson(valueCaptor.getValue()).node("active").isBoolean().isFalse();
        assertThat(keyCaptor.getValue()).isEqualTo("person:test-id");
    }

    @Test
    public void deleting_missing_person_does_nothing() {
        var mockConnection = mock(RedisConnection.class);
        when(mockConnection.get("person:test-id")).thenReturn(null);

        var repo = new RedisPersonRepository(mockConnection, null);
        repo.deletePerson("test-id");

        verify(mockConnection).get("person:test-id");
        verify(mockConnection, times(0)).set(anyString(), anyString());
    }

    @Test
    public void getPersonbyId_returns_null_for_inactive_person() {
        var mockConnection = mock(RedisConnection.class);
        when(mockConnection.get("person:test-id")).thenReturn("{ \"id\": \"test-id\", \"name\": { \"name\": \"Test Mann\" }, \"active\": false }");

        var repo = new RedisPersonRepository(mockConnection, null);
        var result = repo.getPersonById("test-id");

        assertThat(result).isNull();
    }

    @Test
    public void querying_all_person_ids_must_not_list_inactive_person() {
        var mockConnection = mock(RedisConnection.class);
        when(mockConnection.getMembersOfSet("person:ids")).thenReturn(Stream.of("a", "b", "c").collect(Collectors.toSet()));
        when(mockConnection.get("person:a")).thenReturn("{ \"id\": \"a\", \"name\": { \"name\": \"Test Mann\" }, \"active\": true }");
        when(mockConnection.get("person:b")).thenReturn("{ \"id\": \"b\", \"name\": { \"name\": \"Test Mann\" }, \"active\": false }");
        when(mockConnection.get("person:c")).thenReturn("{ \"id\": \"c\", \"name\": { \"name\": \"Test Mann\" }, \"active\": true }");

        var repo = new RedisPersonRepository(mockConnection, mock(IdGenerator.class));
        var result = repo.queryAllPersonIds();

        assertThat(result).containsExactlyInAnyOrder("a", "c");
    }

    @Test
    public void newly_created_person_is_active() {
        var mockConnection = mock(RedisConnection.class);
        var mockIdProvider = mock(IdGenerator.class);
        when(mockIdProvider.generateId()).thenReturn("abcd");
        var repo = new RedisPersonRepository(mockConnection, mockIdProvider);

        var result = repo.createPerson();

        assertThat(result.isActive()).isTrue();
    }
}
