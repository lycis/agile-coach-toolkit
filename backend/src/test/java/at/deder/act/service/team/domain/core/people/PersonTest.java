package at.deder.act.service.team.domain.core.people;

import at.deder.act.service.team.domain.framework.DomainEvent;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PersonTest {
    @Test
    public void valid_name_can_be_set() {
        var person = new Person();

        person.setName(new Name("Max Mustermann"));

        assertThat(person.getName()).isEqualTo(new Name("Max Mustermann"));
    }

    @Test
    public void null_can_not_be_used_as_name() {
        var person = new Person();

        assertThrows(NullPointerException.class, () -> person.setName(null));
    }

    @Test
    public void any_string_is_a_valid_id() {
        var person = new Person();

        person.setId("foo");

        assertThat(person.getId()).isEqualTo("foo");
    }

    @Test
    public void null_is_not_a_valid_id() {
        var person = new Person();
        assertThrows(NullPointerException.class, () -> person.setId(null));
    }

    @Test
    public void deactivating_a_person_raises_event() {
        var person = new Person();
        person.setActive(true);
        person.setId("test-id");

        person.setActive(false);

        assertThat(person.getRaisedDomainEvents()).hasSize(1);

        var event = (PersonDeactivatedEvent) person.getRaisedDomainEvents().get(0);
        assertThat(event).isInstanceOf(PersonDeactivatedEvent.class);
        assertThat(event.getPersonId()).isEqualTo("test-id");
    }

    @Test
    public void deactivating_an_inactive_person_raises_no_event() {
        var person = new Person();
        person.setActive(false);
        person.setId("test-id");

        person.setActive(false);

        assertThat(person.getRaisedDomainEvents()).hasSize(0);
    }
}
