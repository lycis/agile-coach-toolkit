package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamBuilder;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import spark.Request;
import spark.Response;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class CreateTeamHandlerTest {
    @Test
    public void putting_team_with_name_executes_create_team_command() {
        final String TEAM_NAME = "test name";

        TeamRepository mockRepository = mock(TeamRepository.class);
        var team = new TeamBuilder(TEAM_NAME).build();
        team.setId("test-id");
        when(mockRepository.createTeam(TEAM_NAME)).thenReturn(team);
        Request mockRequest = mock(Request.class);
        when(mockRequest.body()).thenReturn(String.format("{name:\"%s\"}", TEAM_NAME));
        Response mockResponse = mock(Response.class);

        var handler = new CreateTeamHandler(mockRepository, mock(DomainEventDispatcher.class));
        handler.handleRequest(mockRequest, mockResponse);

        verify(mockResponse).status(HttpStatus.OK_200);
        verify(mockRepository).createTeam(TEAM_NAME);

        ArgumentCaptor<Team> teamCaptor = ArgumentCaptor.forClass(Team.class);
        verify(mockRepository).saveTeam(teamCaptor.capture());
        var capturedTeam = teamCaptor.getValue();
        assertThat(capturedTeam, notNullValue());
        assertThat(capturedTeam.getName(), is(TEAM_NAME));

        ArgumentCaptor<String> bodyCaptor = ArgumentCaptor.forClass(String.class);
        verify(mockResponse).body(bodyCaptor.capture());
        var body = bodyCaptor.getValue();
        assertThat(body, isJson());
        assertThat(body, hasJsonPath("$.id", is(capturedTeam.getId())));
    }

     // TODO test for error options...
}