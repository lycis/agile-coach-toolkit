package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.query.TeamQueryService;
import at.deder.act.service.team.domain.query.TeamQueryServiceImpl;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class GetTeamNameHandlerTest {

    private TeamQueryService mockQueryService;
    private Request mockRequest;
    private Response mockResponse;

    @Test
    public void get_team_name_of_existing_team_results_in_200_with_name_as_content() {
        instantiateMocks();

        when(mockQueryService.queryNameForTeam("test-id")).thenReturn("team name");
        when(mockRequest.params("id")).thenReturn("test-id");

        var handler = new GetTeamnameHandler(mockQueryService);
        var result = handler.handle(mockRequest, mockResponse);

        assertThat(result, is("team name"));
        verify(mockResponse).status(HttpStatus.OK_200);
        verify(mockQueryService).queryNameForTeam("test-id");
        verify(mockRequest).params("id");
    }

    private void instantiateMocks() {
        mockQueryService = mock(TeamQueryServiceImpl.class);
        mockRequest = mock(Request.class);
        mockResponse = mock(Response.class);
    }

    @Test
    public void get_team_name_of_nonexistant_team_returns_not_found() {
        instantiateMocks();

        when(mockQueryService.queryNameForTeam("test-id")).thenReturn(null);
        when(mockRequest.params("id")).thenReturn("test-id");

        var handler = new GetTeamnameHandler(mockQueryService);
        handler.handle(mockRequest, mockResponse);

        verify(mockResponse).status(HttpStatus.NOT_FOUND_404);
        verify(mockQueryService).queryNameForTeam("test-id");
        verify(mockRequest).params("id");
    }

    @Test
    public void get_team_name_without_id_fails() {
        instantiateMocks();

        when(mockRequest.params("id")).thenReturn(null);

        var handler = new GetTeamnameHandler(mockQueryService);
        handler.handle(mockRequest, mockResponse);

        verify(mockResponse).status(HttpStatus.BAD_REQUEST_400);
        verify(mockRequest).params("id");
    }

     // TODO test rest api conform errors (assertRestApiError)
}
