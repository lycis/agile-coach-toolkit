package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import at.deder.act.service.team.domain.core.team.TeamCreatedEvent;
import at.deder.act.service.team.domain.framework.DomainEvent;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class DeactivatePersonTest {
    @Test
    public void deactivating_an_existing_person_deletes_person_in_repository() {
        var mockPerson = mock(Person.class);
        when(mockPerson.isActive()).thenReturn(true);
        var repo = buildMockRepository(mockPerson);
        var command = buildCommand(repo);

        command.execute();

        var captor = ArgumentCaptor.forClass(String.class);
        verify(repo).deletePerson(captor.capture());
        assertThat(captor.getValue()).isEqualTo("test-id");
    }

    @Test
    public void deactivating_non_existing_person_does_nothing() {
        var repo = buildMockRepository(null);
        var command = buildCommand(repo);

        command.execute();

        verify(repo, times(0)).deletePerson(anyString());
    }

    @Test
    public void deactivating_inactive_person_does_nothing() {
        Person mockInactivePerson = mock(Person.class);
        when(mockInactivePerson.isActive()).thenReturn(false);
        var repo = buildMockRepository(mockInactivePerson);
        var command = buildCommand(repo);

        command.execute();

        verify(repo, times(0)).deletePerson(anyString());
    }

    @Test
    public void all_events_are_dispatched_after_the_command_was_executed_sucessfully() {
        var eventA = new DomainEvent(){};
        var eventB = new DomainEvent(){};
        Person mockPerson = mockPersonWithEvents(Arrays.asList(eventA, eventB), true);
        var mockRepo = buildMockRepository(mockPerson);
        var mockDispatcher = mock(DomainEventDispatcher.class);

        var command = buildCommand(mockRepo, mockDispatcher);
        command.execute();

        var captor = ArgumentCaptor.forClass(DomainEvent.class);
        verify(mockDispatcher, times(2)).dispatch(captor.capture());
        assertThat(captor.getAllValues()).contains(eventA, eventB);
    }

    @Test
    public void all_events_are_dispatched_for_inactive_person() {
        var eventA = new DomainEvent(){};
        var eventB = new DomainEvent(){};
        Person mockPerson = mockPersonWithEvents(Arrays.asList(eventA, eventB), false);
        var mockRepo = buildMockRepository(mockPerson);
        var mockDispatcher = mock(DomainEventDispatcher.class);

        var command = buildCommand(mockRepo, mockDispatcher);
        command.execute();

        var captor = ArgumentCaptor.forClass(DomainEvent.class);
        verify(mockDispatcher, times(2)).dispatch(captor.capture());
        assertThat(captor.getAllValues()).contains(eventA, eventB);
    }

    private Person mockPersonWithEvents(List<DomainEvent> domainEvents, boolean isActive) {
        Person mockPerson = mock(Person.class);
        when(mockPerson.getRaisedDomainEvents()).thenReturn(domainEvents);
        when(mockPerson.isActive()).thenReturn(isActive);
        return mockPerson;
    }

    private DeactivatePersonCommand buildCommand(PersonRepository mockRepo, DomainEventDispatcher mockDispatcher) {
        return new DeactivatePersonCommand(mockRepo, mockDispatcher,"test-id");
    }

    private DeactivatePersonCommand buildCommand(PersonRepository repo) {
        return buildCommand(repo, mock(DomainEventDispatcher.class));
    }

    private PersonRepository buildMockRepository(Person mockPerson) {
        var repo = mock(PersonRepository.class);
        when(repo.getPersonById("test-id")).thenReturn(mockPerson);
        return repo;
    }
}
