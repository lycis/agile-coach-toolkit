package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamBuilder;
import at.deder.act.service.team.domain.core.team.TeamCreatedEvent;
import at.deder.act.service.team.domain.framework.DomainEvent;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class RegisterNewTeamTest {

    @Test
    public void registering_new_team_creates_team_in_repo_and_sets_name() {
        var team = new TeamBuilder("a new team").build();
        var repo = mock(TeamRepository.class);
        when(repo.createTeam("a new team")).thenReturn(team);
        var mockDispatcher = mock(DomainEventDispatcher.class);

        var command = new RegisterNewTeam(repo, "a new team", mockDispatcher);
        var result = command.register();

        verify(repo).createTeam("a new team");
        assertThat(result.getName()).isEqualTo("a new team");
    }

    @Test
    public void registering_new_team_dispatches_all_pending_events() {
        var team = mock(Team.class);
        var eventA = new DomainEvent(){};
        var eventB = new DomainEvent(){};
        when(team.getRaisedDomainEvents()).thenReturn(Arrays.asList(eventA, eventB));

        var mockDispatcher = mock(DomainEventDispatcher.class);
        var mockRepository = mock(TeamRepository.class);
        when(mockRepository.createTeam("foo")).thenReturn(team);

        var command = new RegisterNewTeam(mockRepository, "foo", mockDispatcher);
        command.register();

        var captor = ArgumentCaptor.forClass(DomainEvent.class);
        verify(mockDispatcher, times(3)).dispatch(captor.capture());
        assertThat(captor.getAllValues()).contains(eventA, eventB);
    }

    @Test
    public void registering_new_team_raises_created_event() {
        var team = mock(Team.class);
        when(team.getId()).thenReturn("test-id");
        var mockDispatcher = mock(DomainEventDispatcher.class);
        var mockRepository = mock(TeamRepository.class);
        when(mockRepository.createTeam("foo")).thenReturn(team);

        var command = new RegisterNewTeam(mockRepository, "foo", mockDispatcher);
        command.register();

        var captor = ArgumentCaptor.forClass(TeamCreatedEvent.class);
        verify(mockDispatcher).dispatch(captor.capture());
        assertThat(captor.getValue().getId()).isEqualTo("test-id");
    }
}
