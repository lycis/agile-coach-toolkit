package at.deder.act.service.team.domain.core.team;

import java.lang.reflect.Member;

public class TeamBuilder {
    Team team;

    public TeamBuilder(String name) {
        team = Team.create(name);
    }

    public TeamBuilder withId(String id) {
        team.setId(id);
        return this;
    }

    public TeamBuilder withMembers(Iterable<Member> members) {
        for (var member : members) {

            team.addMember(member.getName());
        }
        return this;
    }

    public TeamBuilder withMember(Member member) {
        team.addMember(member.getName());
        return this;
    }

    public TeamBuilder withNotes(Iterable<Note> notes) {
        for (var note : notes) {

            team.addNote(note);
        }
        return this;
    }

    public TeamBuilder withNote(Note note) {
        team.addNote(note);
        return this;
    }

    public Team build() {
        return team;
    }

}
