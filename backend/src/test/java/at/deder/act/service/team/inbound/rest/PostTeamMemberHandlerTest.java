package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import spark.Request;
import spark.Response;

import static at.deder.act.service.team.inbound.rest.RestApiAssertions.assertRestApiError;
import static org.mockito.Mockito.*;

public class PostTeamMemberHandlerTest {
    private Request request;
    private Response response;
    private TeamRepository mockTeamRepo;
    private PersonRepository mockPersonRepo;
    private Team mockTeam;
    private Person mockPerson;

    @Test
    public void add_new_existing_member_to_existing_team_adds_member_to_the_team() {
        arrangeMocksInDefaultConfiguration();

        var handler = buildHandler();
        handler.handle(request, response);

        verify(mockTeam).addMember("person-id");
        verify(response).status(HttpStatus.OK_200);
    }

    @Test
    public void add_not_existing_member_to_existing_team() {
        arrangeMocksInDefaultConfiguration();
        when(mockPersonRepo.getPersonById("person-id")).thenReturn(null);

        var handler = buildHandler();

        verify(mockTeam, times(0)).addMember(anyString());
        assertRestApiError(() -> handler.handle(request, response), HttpStatus.UNPROCESSABLE_ENTITY_422, "Person does not exist", "The given person-id does not map to an existing or active person.");
    }

    @Test
    public void add_existing_member_not_existing_team_returns_team_not_found() {
        arrangeMocksInDefaultConfiguration();
        when(mockTeamRepo.findTeam("team-id")).thenReturn(null);

        var handler = buildHandler();

        assertRestApiError(() -> handler.handle(request, response), HttpStatus.NOT_FOUND_404, "Team not found", "A team with the given team-id does not exist");
    }

    private void arrangeMocksInDefaultConfiguration() {
        createMocks();
        arrangeMocksToBeBasicallyWired();
    }

    private void arrangeMocksToBeBasicallyWired() {
        when(request.params("team")).thenReturn("team-id");
        when(request.params("person")).thenReturn("person-id");
        when(mockTeamRepo.findTeam("team-id")).thenReturn(mockTeam);
        when(mockPersonRepo.getPersonById("person-id")).thenReturn(mockPerson);
    }

    private void createMocks() {
        request = mock(Request.class);
        response = mock(Response.class);
        mockTeamRepo = mock(TeamRepository.class);
        mockPersonRepo = mock(PersonRepository.class);
        mockTeam = mock(Team.class);
        mockPerson = mock(Person.class);
    }

    private PostTeamMemberHandler buildHandler() {
        return new PostTeamMemberHandler(mockTeamRepo, mockPersonRepo, mock(DomainEventDispatcher.class));
    }

}
