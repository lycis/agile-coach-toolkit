package at.deder.act.service.team.domain.command;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import at.deder.act.service.team.domain.core.team.Team;
import at.deder.act.service.team.domain.core.team.TeamRepository;
import at.deder.act.service.team.domain.framework.DomainEvent;
import at.deder.act.service.team.domain.framework.DomainEventDispatcher;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class AddTeamMemberTest {

    @Test
    public void adding_new_member_to_an_existing_team_appends_it_to_the_member_list() {
        var mockTeamRepository = mock(TeamRepository.class);
        var mockPersonRepository = mock(PersonRepository.class);
        var mockTeam = mock(Team.class);
        var mockPerson = mock(Person.class);

        when(mockTeamRepository.findTeam("team-id")).thenReturn(mockTeam);
        when(mockPersonRepository.getPersonById("person-id")).thenReturn(mockPerson);

        AddTeamMember command = buildCommandWithMember(mockTeamRepository, mockPersonRepository);
        command.execute();

        var captor = ArgumentCaptor.forClass(String.class);
        verify(mockTeam).addMember(captor.capture());
        assertThat(captor.getValue()).isEqualTo("person-id");
        verify(mockTeamRepository).saveTeam(mockTeam);
    }

    @Test
    public void adding_nonexistant_member_to_team_throws_error() {
        var mockTeamRepository = mock(TeamRepository.class);
        var mockPersonRepository = mock(PersonRepository.class);
        var mockTeam = mock(Team.class);

        when(mockTeamRepository.findTeam("team-id")).thenReturn(mockTeam);
        when(mockPersonRepository.getPersonById("person-id")).thenReturn(null);

        AddTeamMember command = buildCommandWithMember(mockTeamRepository, mockPersonRepository);
        var exception = assertThrows(PersonNotFoundException.class, command::execute);
        assertThat(exception.getMessage()).isEqualTo(String.format("Person (%s) does not exist", "person-id"));
    }

    @Test
    public void adding_member_to_not_existing_team_throws_error() {
        var mockTeamRepository = mock(TeamRepository.class);
        var mockPersonRepository = mock(PersonRepository.class);
        var mockPerson = mock(Person.class);

        when(mockTeamRepository.findTeam("team-id")).thenReturn(null);
        when(mockPersonRepository.getPersonById("person-id")).thenReturn(mockPerson);

        AddTeamMember command = buildCommandWithMember(mockTeamRepository, mockPersonRepository);
        var exception = assertThrows(TeamNotFoundException.class, command::execute);
        assertThat(exception.getMessage()).isEqualTo(String.format("Team (%s) does not exist", "team-id"));
    }

    @Test
    public void adding_multiple_members_in_one_execution_adds_all_of_them() {
        var mockTeamRepository = mock(TeamRepository.class);
        var mockPersonRepository = mock(PersonRepository.class);
        var mockPersonA = mock(Person.class);
        var mockPersonB = mock(Person.class);
        var mockPersonC = mock(Person.class);
        var mockTeam    = mock(Team.class);

        when(mockTeamRepository.findTeam("team-id")).thenReturn(mockTeam);
        when(mockPersonRepository.getPersonById("person-a")).thenReturn(mockPersonA);
        when(mockPersonRepository.getPersonById("person-b")).thenReturn(mockPersonB);
        when(mockPersonRepository.getPersonById("person-c")).thenReturn(mockPersonC);

        var command = buildCommandWithMembers(mockTeamRepository, mockPersonRepository, "person-a", "person-b", "person-c");
        command.execute();

        var captor = ArgumentCaptor.forClass(String.class);
        verify(mockTeam, times(3)).addMember(captor.capture());
        assertThat(captor.getAllValues()).contains("person-a", "person-b", "person-c");
        verify(mockTeamRepository).saveTeam(mockTeam);
    }

    @Test
    public void all_events_are_dispatched_for_inactive_person() {
        var eventA = new DomainEvent(){};
        var eventB = new DomainEvent(){};
        var mockDispatcher = mock(DomainEventDispatcher.class);
        var mockTeamRepository = mock(TeamRepository.class);
        var mockPersonRepository = mock(PersonRepository.class);
        var mockTeam    = mock(Team.class);
        var mockPerson = mock(Person.class);

        when(mockTeamRepository.findTeam("team-id")).thenReturn(mockTeam);
        when(mockPersonRepository.getPersonById("person-id")).thenReturn(mockPerson);
        when(mockTeam.getRaisedDomainEvents()).thenReturn(Arrays.asList(eventA, eventB));

        var command = buildCommandWithMember(mockTeamRepository, mockPersonRepository, mockDispatcher);
        command.execute();

        var captor = ArgumentCaptor.forClass(DomainEvent.class);
        verify(mockDispatcher, times(2)).dispatch(captor.capture());
        assertThat(captor.getAllValues()).contains(eventA, eventB);
    }

    private AddTeamMember buildCommandWithMember(TeamRepository mockTeamRepository, PersonRepository mockPersonRepository) {
        return buildCommandWithMembers(mockTeamRepository, mockPersonRepository, mock(DomainEventDispatcher.class),"person-id");
    }

    private AddTeamMember buildCommandWithMember(TeamRepository mockTeamRepository, PersonRepository mockPersonRepository, DomainEventDispatcher mockDispatcher) {
        return buildCommandWithMembers(mockTeamRepository, mockPersonRepository, mockDispatcher, "person-id");
    }

    private AddTeamMember buildCommandWithMembers(TeamRepository mockTeamRepository, PersonRepository mockPersonRepository, String... people) {
        return buildCommandWithMembers(mockTeamRepository, mockPersonRepository, mock(DomainEventDispatcher.class), people);
    }

    private AddTeamMember buildCommandWithMembers(TeamRepository mockTeamRepository, PersonRepository mockPersonRepository, DomainEventDispatcher mockDispatcher, String... people) {
        var command = buildCommand(mockTeamRepository, mockPersonRepository, mockDispatcher);
        Arrays.stream(people).forEach(p -> command.addMemberToTeam("team-id", p));
        return command;
    }

    private AddTeamMember buildCommand(TeamRepository mockTeamRepository, PersonRepository mockPersonRepository, DomainEventDispatcher mockDispatcher) {
        return new AddTeamMember(mockTeamRepository, mockPersonRepository, mockDispatcher);
    }

}
