package at.deder.act.service.team.inbound.rest;

import net.javacrumbs.jsonunit.assertj.JsonAssertions;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.function.Executable;
import spark.HaltException;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class RestApiAssertions {
    static void assertRestApiError(HaltException exception, int expectedStatus, String expectedTitle, String expectedDescription) {
        Assertions.assertThat(exception.statusCode()).isEqualTo(expectedStatus);
        JsonAssertions.assertThatJson(exception.body()).inPath("$.title").isEqualTo(expectedTitle);
        JsonAssertions.assertThatJson(exception.body()).inPath("$.detail").isEqualTo(expectedDescription);
    }

    static void assertRestApiError(Executable call, int expectedStatus, String expectedTitle, String expectedDescription) {
        var exception = assertThrows(HaltException.class, call);
        assertRestApiError(exception, expectedStatus, expectedTitle, expectedDescription);
    }
}