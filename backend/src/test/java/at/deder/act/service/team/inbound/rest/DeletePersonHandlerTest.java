package at.deder.act.service.team.inbound.rest;

import at.deder.act.service.team.domain.core.people.Person;
import at.deder.act.service.team.domain.core.people.PersonRepository;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.Test;
import spark.HaltException;
import spark.Request;
import spark.Response;

import static at.deder.act.service.team.inbound.rest.RestApiAssertions.assertRestApiError;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class DeletePersonHandlerTest {
    @Test
    public void delete_existing_person_deletes_and_returns_200() {
        var request = mock(Request.class);
        var response = mock(Response.class);
        var mockRepository = mock(PersonRepository.class);
        var mockPerson = mock(Person.class);

        when(request.params("id")).thenReturn("test-id");
        when(mockRepository.getPersonById("test-id")).thenReturn(mockPerson);
        when(mockPerson.isActive()).thenReturn(true);

        var handler = new DeletePersonHandler(mockRepository);
        handler.handle(request, response);

        verify(response).status(HttpStatus.OK_200);
        verify(mockRepository).deletePerson("test-id");
    }

    @Test
    public void deleting_nonexistant_person_returns_not_found() {
        var request = mock(Request.class);
        var response = mock(Response.class);
        var mockRepository = mock(PersonRepository.class);

        when(request.params("id")).thenReturn("does-not-exist");
        when(mockRepository.getPersonById("does-not-exist")).thenReturn(null);

        var handler = new DeletePersonHandler(mockRepository);
        var exception = assertThrows(HaltException.class, () -> handler.handle(request, response));

        assertThat(exception.statusCode()).isEqualTo(HttpStatus.NOT_FOUND_404);
        verify(mockRepository, times(0)).deletePerson(anyString());
    }

    @Test
    public void return_error_when_id_is_missing() {
        var request = mock(Request.class);
        var response = mock(Response.class);
        var mockRepository = mock(PersonRepository.class);

        when(request.params("id")).thenReturn("");

        var handler = new DeletePersonHandler(mockRepository);
        assertRestApiError(() -> handler.handle(request, response), HttpStatus.BAD_REQUEST_400, "Missing ID", "An ID of an existing person is required.");
    }

    @Test
    public void return_error_when_person_is_inactive() {
        var request = mock(Request.class);
        var response = mock(Response.class);
        var mockRepository = mock(PersonRepository.class);
        var mockPerson = mock(Person.class);

        when(request.params("id")).thenReturn("test-id");
        when(mockRepository.getPersonById("test-id")).thenReturn(mockPerson);
        when(mockPerson.isActive()).thenReturn(false);

        var handler = new DeletePersonHandler(mockRepository);
        assertRestApiError(() -> handler.handle(request, response), HttpStatus.NOT_FOUND_404, "Person is already inactive", "The requested person is already inactive.");
    }
}
